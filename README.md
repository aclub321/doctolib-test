# README

By using this application, user can book appointment(s) with doctors. Each doctor has given his/her opening hours. The opening hours are divided into 30 minutes slots. The user can book an appointment for atleast 30 minutes or multiple of 30 minute slots i.e 1:30 hour appointment means 3 * 30 minutes slots. 

The challenge is to book appointment slot without overlapping other appointment slots. Also, had to take care if appoinments are made within the opening hours.

### Note
This application does not have user interface. The only way to test application is by running tests or looking into the code.

## Files containing code and related tests
* app/models/event.rb
* test/models/event_test.rb

## Ruby version
2.7.3

## Running tests
Enter following command at command prompt to run tests

`rake`

