class Event < ApplicationRecord
  scope :filter_event, (lambda do |event_kind|
    where(:kind => event_kind)
  end)

  scope :filter_dates, (lambda do |start_date, end_date|
    where(:starts_at => start_date.beginning_of_day..end_date.end_of_day)
  end)

  def self.availabilities(start_date)
    end_date = start_date + 6.days

    date_arr = (start_date..end_date).to_a

    availabilities = initialize_availabilities(date_arr)

    # Generate opening slots
    generate_opening_slots(availabilities, date_arr)

    # Subtract appointments from opening slots
    subtract_appointment_slots(availabilities, date_arr)

    availabilities
  end

  def self.generate_opening_slots(availabilities, date_arr)
    date_arr.each_with_index do |date, index|
      opening_events = get_opening_events(date)

      add_opening_by_date(availabilities, index, opening_events)
    end
  end

  def self.get_opening_events(date)
    # Find opening event for specific date OR
    # If opening event does not exists for specified date then search opening 
    # event with weekly_recurring and get latest event for that week day 
    Event.filter_event('opening')
         .where(
           "(starts_at >= :from_date AND starts_at <= :to_date) " \
           "OR " \
           "(starts_at < :from_date AND " \
           " weekly_recurring = 1 AND " \
           " strftime('%w', starts_at) = ':week_day')",
           {
             :from_date => date.beginning_of_day,
             :to_date => date.end_of_day,
             :week_day => date.wday
           }
         )
         .order(:starts_at => :desc)
         .limit(2)# Assuming that there can be no more than 2 openings per day
  end

  def self.subtract_appointment_slots(availabilities, date_arr)
    # Get appointment events for 7 days
    appointment_events = Event.filter_event('appointment')
                              .filter_dates(date_arr[0], date_arr[6])
                              .order(:starts_at)

  
    subtract_appointments_by_date(availabilities, appointment_events) 
  end

  # deduct appointment slots from availabilities
  def self.subtract_appointments_by_date(availabilities, events)
    index = 0
    
    events.each do |event|
      # Loop until we reach availabilities[index] that corrresponds to 
      # event date
      loop do 
        event_start_date = event.starts_at.strftime('%Y/%m/%d')      

        break if event_start_date == availabilities[index][:date]

        index += 1
      end

      # subtract appointment time slots from slots array
      availabilities[index][:slots] -= generate_slots(event.starts_at, 
                                                      event.ends_at) 
    end
  end

  # add opening slots to availabilities
  def self.add_opening_by_date(availabilities, index, events)
    slots = []

    return if events.blank?

    date_str = events[0].starts_at.strftime('%Y/%m/%d')

    events.each do |event|
      # loop uptil events from same date
      break if event.starts_at.strftime('%Y/%m/%d') != date_str
      
      # Add time to slots array
      slots += generate_slots(event.starts_at, event.ends_at) 
    end

    # Sort by length and then alphabetically
    availabilities[index][:slots] = slots.sort_by{ |s| [s.length, s] }
  end

  def self.generate_slots(start_time, end_time)
    slots = []

    loop do
      time_str = start_time.strftime('%-k:%M')

      slots.push(time_str)

      start_time += 30.minutes

      break if start_time >= end_time
    end

    slots
  end

  def self.initialize_availabilities(date_arr)
    availabilities = Array.new(date_arr.length)

    date_arr.each_with_index do |date, index|
      availabilities[index] = {
        :date => date.strftime('%Y/%m/%d'),
        :slots => []
      }
    end

    availabilities
  end
end
