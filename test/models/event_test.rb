require 'test_helper'

class EventTest < ActiveSupport::TestCase
  test "gets correct opening slots without weekly_recurring" do
    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-01 10:30"), 
                 ends_at: DateTime.parse("2020-10-01 11:30")

    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-03 09:00"), 
                 ends_at: DateTime.parse("2020-10-03 11:00")

    start_date = DateTime.parse("2020-10-01")
    end_date = start_date + 6.days

    date_arr = (start_date..end_date).to_a

    availabilities = Event.initialize_availabilities(date_arr)

    Event.generate_opening_slots(availabilities, date_arr)

    assert_equal '2020/10/01', availabilities[0][:date]
    assert_equal ["10:30", "11:00"], availabilities[0][:slots]
    assert_equal '2020/10/02', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]
    assert_equal '2020/10/03', availabilities[2][:date]
    assert_equal ["9:00", "9:30", "10:00", "10:30"], 
                 availabilities[2][:slots]
    assert_equal '2020/10/04', availabilities[3][:date]
    assert_equal [], availabilities[3][:slots]
    assert_equal '2020/10/05', availabilities[4][:date]
    assert_equal [], availabilities[4][:slots]
    assert_equal '2020/10/06', availabilities[5][:date]
    assert_equal [], availabilities[5][:slots]
    assert_equal '2020/10/07', availabilities[6][:date]
    assert_equal [], availabilities[6][:slots]

    assert_equal 7, availabilities.length
  end

  test "gets correct opening slots when multiple openings per day" do
    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-01 09:30"), 
                 ends_at: DateTime.parse("2020-10-01 11:30")

    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-01 15:00"), 
                 ends_at: DateTime.parse("2020-10-01 17:00")

    start_date = DateTime.parse("2020-10-01")
    end_date = start_date + 6.days

    date_arr = (start_date..end_date).to_a

    availabilities = Event.initialize_availabilities(date_arr)

    Event.generate_opening_slots(availabilities, date_arr)

    assert_equal '2020/10/01', availabilities[0][:date]
    assert_equal ["9:30", "10:00", "10:30", "11:00", "15:00", "15:30", "16:00", "16:30"], availabilities[0][:slots]
    assert_equal '2020/10/02', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]
    assert_equal '2020/10/07', availabilities[6][:date]
    assert_equal [], availabilities[6][:slots]

    assert_equal 7, availabilities.length
  end

  test "gets correct opening slots when weekly-recurring used" do
    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-01 10:30"), 
                 ends_at: DateTime.parse("2020-10-01 11:30"),
                 weekly_recurring: true

    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-22 09:00"), 
                 ends_at: DateTime.parse("2020-10-22 11:00"),
                 weekly_recurring: true

    start_date = DateTime.parse("2020-10-01")
    end_date = start_date + 6.days

    date_arr = (start_date..end_date).to_a

    availabilities = Event.initialize_availabilities(date_arr)

    Event.generate_opening_slots(availabilities, date_arr)

    assert_equal '2020/10/01', availabilities[0][:date]
    assert_equal ["10:30", "11:00"], availabilities[0][:slots]
    assert_equal '2020/10/02', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]
    assert_equal 7, availabilities.length

    start_date = DateTime.parse("2020-10-08")
    end_date = start_date + 6.days

    date_arr = (start_date..end_date).to_a

    availabilities = Event.initialize_availabilities(date_arr)

    Event.generate_opening_slots(availabilities, date_arr)
    assert_equal '2020/10/08', availabilities[0][:date]
    assert_equal ["10:30", "11:00"], availabilities[0][:slots]
    assert_equal '2020/10/09', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]
    assert_equal 7, availabilities.length
  end

  test "gets correct opening slots when multiple openings per day and weekly-recurring used" do
    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-01 10:30"), 
                 ends_at: DateTime.parse("2020-10-01 11:30"),
                 weekly_recurring: true

    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-01 15:00"), 
                 ends_at: DateTime.parse("2020-10-01 16:00"),
                 weekly_recurring: true

    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-22 09:00"), 
                 ends_at: DateTime.parse("2020-10-22 11:00"),
                 weekly_recurring: true

    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-22 12:00"), 
                 ends_at: DateTime.parse("2020-10-22 13:30"),
                 weekly_recurring: true

    start_date = DateTime.parse("2020-10-01")
    end_date = start_date + 6.days

    date_arr = (start_date..end_date).to_a

    availabilities = Event.initialize_availabilities(date_arr)

    Event.generate_opening_slots(availabilities, date_arr)

    assert_equal '2020/10/01', availabilities[0][:date]
    assert_equal ["10:30", "11:00", "15:00", "15:30"], availabilities[0][:slots]
    assert_equal '2020/10/02', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]
    assert_equal 7, availabilities.length

    start_date = DateTime.parse("2020-10-08")
    end_date = start_date + 6.days

    date_arr = (start_date..end_date).to_a

    availabilities = Event.initialize_availabilities(date_arr)

    Event.generate_opening_slots(availabilities, date_arr)
    assert_equal '2020/10/08', availabilities[0][:date]
    assert_equal ["10:30", "11:00", "15:00", "15:30"], availabilities[0][:slots]
    assert_equal '2020/10/09', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]
    assert_equal 7, availabilities.length
  end

  test "gets correct opening slots when several weekly-recurring used for same weekday" do
    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-01 10:30"), 
                 ends_at: DateTime.parse("2020-10-01 11:30"),
                 weekly_recurring: true

    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-01 14:30"), 
                 ends_at: DateTime.parse("2020-10-01 15:30"),
                 weekly_recurring: true

    Event.create kind: 'opening', 
                 starts_at: DateTime.parse("2020-10-22 09:00"), 
                 ends_at: DateTime.parse("2020-10-22 11:00"),
                 weekly_recurring: true

    start_date = DateTime.parse("2020-10-22")
    end_date = start_date + 6.days

    date_arr = (start_date..end_date).to_a

    availabilities = Event.initialize_availabilities(date_arr)

    Event.generate_opening_slots(availabilities, date_arr)

    assert_equal '2020/10/22', availabilities[0][:date]
    assert_equal ["9:00", "9:30", "10:00", "10:30"], availabilities[0][:slots]
    assert_equal '2020/10/23', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]


    start_date = DateTime.parse("2020-10-29")
    end_date = start_date + 6.days

    date_arr = (start_date..end_date).to_a

    availabilities = Event.initialize_availabilities(date_arr)

    Event.generate_opening_slots(availabilities, date_arr)
    assert_equal '2020/10/29', availabilities[0][:date]
    assert_equal ["9:00", "9:30", "10:00", "10:30"], availabilities[0][:slots]
    assert_equal '2020/10/30', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]

    assert_equal 7, availabilities.length
  end

  # TESTS givemn from Doclib
  test "one simple test example" do
    Event.create kind: 'opening', starts_at: DateTime.parse("2014-08-04 09:30"), ends_at: DateTime.parse("2014-08-04 13:30"), weekly_recurring: true
    Event.create kind: 'appointment', starts_at: DateTime.parse("2014-08-11 10:30"), ends_at: DateTime.parse("2014-08-11 11:30")

    availabilities = Event.availabilities DateTime.parse("2014-08-10")
    assert_equal '2014/08/10', availabilities[0][:date]
    assert_equal [], availabilities[0][:slots]
    assert_equal '2014/08/11', availabilities[1][:date]
    assert_equal ["9:30", "10:00", "11:30", "12:00", "12:30", "13:00"], availabilities[1][:slots]
    assert_equal [], availabilities[2][:slots]
    assert_equal '2014/08/16', availabilities[6][:date]
    assert_equal 7, availabilities.length
  end

  test "whole day booked on 2020-10-08" do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-01 09:30"),
                 ends_at: DateTime.parse("2020-10-01 13:30"),
                 weekly_recurring: true

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-08 09:30"),
                 ends_at: DateTime.parse("2020-10-08 10:30")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-08 10:30"),
                 ends_at: DateTime.parse("2020-10-08 11:30")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-08 11:30"),
                 ends_at: DateTime.parse("2020-10-08 13:30")

    availabilities = Event.availabilities DateTime.parse("2020-10-07")
    assert_equal '2020/10/07', availabilities[0][:date]
    assert_equal [], availabilities[0][:slots]
    assert_equal '2020/10/08', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]
    assert_equal [], availabilities[2][:slots]
    assert_equal '2020/10/13', availabilities[6][:date]
    assert_equal 7, availabilities.length
  end

  test "check weekly_recurring donot effect previous dates opening times" do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-08 09:30"),
                 ends_at: DateTime.parse("2020-10-08 13:30"),
                 weekly_recurring: true

    availabilities = Event.availabilities DateTime.parse("2020-09-30")
    assert_equal '2020/09/30', availabilities[0][:date]
    assert_equal [], availabilities[0][:slots]
    assert_equal '2020/10/01', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]
    assert_equal [], availabilities[2][:slots]
    assert_equal '2020/10/06', availabilities[6][:date]
    assert_equal 7, availabilities.length
  end

  test "use correct weekly_recurring time which should not be from past date" do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-01 09:30"),
                 ends_at: DateTime.parse("2020-10-01 13:30"),
                 weekly_recurring: true

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-08 10:30"),
                 ends_at: DateTime.parse("2020-10-08 11:30")

    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-15 09:00"),
                 ends_at: DateTime.parse("2020-10-15 12:00"),
                 weekly_recurring: true

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-22 11:30"),
                 ends_at: DateTime.parse("2020-10-22 12:00")

    availabilities = Event.availabilities DateTime.parse("2020-10-08")
    assert_equal '2020/10/08', availabilities[0][:date]
    assert_equal ["9:30", "10:00", "11:30", "12:00", "12:30", "13:00"],
                 availabilities[0][:slots]

    availabilities = Event.availabilities DateTime.parse("2020-10-15")
    assert_equal '2020/10/15', availabilities[0][:date]
    assert_equal ["9:00", "9:30", "10:00", "10:30", "11:00", "11:30"],
                 availabilities[0][:slots]

    availabilities = Event.availabilities DateTime.parse("2020-10-22")
    assert_equal '2020/10/22', availabilities[0][:date]
    assert_equal ["9:00", "9:30", "10:00", "10:30", "11:00"],
                 availabilities[0][:slots]
  end

  test "correct availability with weekly_recurring when last weekday has diffent timings" do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-05 09:30"),
                 ends_at: DateTime.parse("2020-10-05 13:30"),
                 weekly_recurring: true

    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-05 17:00"),
                 ends_at: DateTime.parse("2020-10-05 18:00"),
                 weekly_recurring: true

    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-12 10:00"),
                 ends_at: DateTime.parse("2020-10-12 12:00")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-19 09:30"),
                 ends_at: DateTime.parse("2020-10-19 10:30")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-19 10:30"),
                 ends_at: DateTime.parse("2020-10-19 11:30")

    availabilities = Event.availabilities DateTime.parse("2020-10-19")
    assert_equal '2020/10/19', availabilities[0][:date]
    assert_equal ["11:30", "12:00", "12:30", "13:00", "17:00", "17:30"], 
                 availabilities[0][:slots]
    assert_equal '2020/10/20', availabilities[1][:date]
    assert_equal [], availabilities[1][:slots]
    assert_equal [], availabilities[2][:slots]
    assert_equal '2020/10/25', availabilities[6][:date]
    assert_equal 7, availabilities.length
  end

  test "availability for whole week" do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-05 09:30"),
                 ends_at: DateTime.parse("2020-10-05 13:30"),
                 weekly_recurring: true

    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-06 09:30"),
                 ends_at: DateTime.parse("2020-10-06 13:30"),
                 weekly_recurring: true

    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-08 09:30"),
                 ends_at: DateTime.parse("2020-10-08 13:30"),
                 weekly_recurring: true

    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-09 10:00"),
                 ends_at: DateTime.parse("2020-10-09 12:00"),
                 weekly_recurring: true

    # Using different opening times than normal on this weekday
    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-13 14:00"),
                 ends_at: DateTime.parse("2020-10-13 16:30")

    # Appointments
    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-13 14:00"),
                 ends_at: DateTime.parse("2020-10-13 14:30")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-13 16:00"),
                 ends_at: DateTime.parse("2020-10-13 16:30")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-15 10:30"),
                 ends_at: DateTime.parse("2020-10-15 11:30")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-16 10:00"),
                 ends_at: DateTime.parse("2020-10-16 10:30")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-16 10:30"),
                 ends_at: DateTime.parse("2020-10-16 11:30")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-16 11:30"),
                 ends_at: DateTime.parse("2020-10-16 12:00")

    availabilities = Event.availabilities DateTime.parse("2020-10-12")
    assert_equal '2020/10/12', availabilities[0][:date]
    assert_equal ["9:30", "10:00", "10:30", "11:00", "11:30", "12:00", 
                  "12:30", "13:00"], availabilities[0][:slots]

    assert_equal '2020/10/13', availabilities[1][:date]
    assert_equal ["14:30", "15:00", "15:30"], availabilities[1][:slots]

    assert_equal '2020/10/14', availabilities[2][:date]
    assert_equal [], availabilities[2][:slots]

    assert_equal '2020/10/15', availabilities[3][:date]
    assert_equal ["9:30", "10:00", "11:30", "12:00", "12:30", "13:00"],
                 availabilities[3][:slots]

    assert_equal '2020/10/16', availabilities[4][:date]
    assert_equal [], availabilities[4][:slots]

    assert_equal '2020/10/17', availabilities[5][:date]
    assert_equal [], availabilities[5][:slots]

    assert_equal '2020/10/18', availabilities[6][:date]
    assert_equal [], availabilities[6][:slots]

    assert_equal 7, availabilities.length
  end

  test "correct availability with first opeining with weekly_recurring and second opening without weekly_recurring" do
    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-05 09:30"),
                 ends_at: DateTime.parse("2020-10-05 13:30"),
                 weekly_recurring: true

    Event.create kind: 'opening',
                 starts_at: DateTime.parse("2020-10-05 17:00"),
                 ends_at: DateTime.parse("2020-10-05 18:00")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-05 17:30"),
                 ends_at: DateTime.parse("2020-10-05 18:00")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-19 09:30"),
                 ends_at: DateTime.parse("2020-10-19 10:30")

    Event.create kind: 'appointment',
                 starts_at: DateTime.parse("2020-10-19 10:30"),
                 ends_at: DateTime.parse("2020-10-19 11:30")

    availabilities = Event.availabilities DateTime.parse("2020-10-05")
    assert_equal '2020/10/05', availabilities[0][:date]
    assert_equal ["9:30", "10:00", "10:30", "11:00", "11:30", "12:00", 
                  "12:30", "13:00", "17:00"], 
                 availabilities[0][:slots]

    availabilities = Event.availabilities DateTime.parse("2020-10-19")
    assert_equal '2020/10/19', availabilities[0][:date]
    assert_equal ["11:30", "12:00", "12:30", "13:00"], 
                 availabilities[0][:slots]
  end
end
